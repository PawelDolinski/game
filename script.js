// --------------------- setting cards

setTimeout(function () {
    var x = document.getElementsByClassName('cards');
    x[0].classList.add('slideInLeft');
    x[1].classList.add('slideInLeft');
    x[1].style.animationDelay = ".3s";
    x[2].style.animationDelay = ".5s";
    x[2].classList.add('slideInLeft');

}, 1000);


//------------------------- shuffling cards
document.body.onload = function () {
    const x = Math.floor((Math.random() * 3) + 1);

    const animation = document.querySelector('.slideInLeft');

    if (x === 1) {
        document.querySelector('.card-1').classList.add('joker');
    } else if (x === 2) {
        document.querySelector('.card-2').classList.add('joker');
    } else if (x === 3) {
        document.querySelector('.card-3').classList.add('joker');
    }
}

document.querySelectorAll('.cards').forEach(function (card) {
        card.addEventListener('click', function () {
            if (card.classList.contains('joker')) {
            
                alert('WIN!')
                document.querySelector('.joker').style.background = "url(img/joker.jpg)center center no-repeat #fff";
                document.querySelector('.joker').style.transform = "rotate3d(0, 10, 0, 180deg)";
                document.querySelector('.joker').style.transition = "1s";
            } else {
            
                alert('LOST')
                document.querySelector('.joker').style.background = "url(img/joker.jpg)center center no-repeat #fff";
                document.querySelector('.joker').style.transform = "rotate3d(0, 10, 0, 180deg)";
                document.querySelector('.joker').style.transition = "1s";

            }
        })
    })
    // --------------------- play again button 
var tryAgain = document.getElementById('btn').addEventListener('click', function () {
    location.reload();
})
